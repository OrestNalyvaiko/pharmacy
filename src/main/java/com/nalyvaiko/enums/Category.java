package com.nalyvaiko.enums;

public enum Category {
  ANTI_INFLAMMATORY,
  ANTIRYRETIC,
  EMACIATION,
  MOISTURIZING,
  TONING_UP,
  SEDATIVE,
  ANTI_ALLERGENIC
}
