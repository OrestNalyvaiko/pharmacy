package com.nalyvaiko.enums;

public enum MedicalForm {
  PILL,
  SYRUP
}
