package com.nalyvaiko.enums;

public enum CosmeticType {
  CREAM,
  LOTION
}
